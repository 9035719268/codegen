package com.gvozdev.codegen.exercise;

public class ExercisePage {
    private final String var;
    private final String language;
    private final boolean isFile;
    private final boolean isOop;

    public ExercisePage(String var, String language, boolean isFile, boolean isOop) {
        this.var = var;
        this.language = language;
        this.isFile = isFile;
        this.isOop = isOop;
    }

    public String getExercisePage() {
        String file = isFile ? "file" : "manual";
        String oop = isOop ? "withoop" : "nooop";
        String exercisePage = "downloadpages/"
                              .concat(var).concat("/")
                              .concat(language).concat("/")
                              .concat(file).concat("/")
                              .concat(oop).concat("/")
                              .concat("download");
        return exercisePage;
    }
}
