package com.gvozdev.codegen.exercise.filereaders;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Var2FileReader {
    private final byte[] fileBytes;
    private final List<List<List<Byte>>> allLines;
    private final int amountOfObservations;

    public Var2FileReader(String fileName) throws IOException {
        String fullFileName = "src/main/resources/files/input/" + fileName;
        FileInputStream fin = new FileInputStream(fullFileName);
        fileBytes = new byte[fin.available()];
        int offset = 0;
        fin.read(fileBytes, offset, fileBytes.length);
        fin.close();
        allLines = analyzeSyntaxAndReturnLinesList();
        amountOfObservations = 360;
    }

    public List<String> getSatelliteNumbers() {
        List<String> satellites = new ArrayList<>();
        for (char satelliteNumber = '1'; satelliteNumber <= '9'; satelliteNumber++) {
            List<List<Double>> measurements = getMeasurements(satelliteNumber, 1);
            if (measurements.size() >= amountOfObservations) {
                String number = String.valueOf(satelliteNumber);
                satellites.add(number);
            }
        }

        for (int satelliteNumber = 10; satelliteNumber <= 40; satelliteNumber++) {
            String satellite = String.valueOf(satelliteNumber);
            char firstDigit = satellite.charAt(0);
            char secondDigit = satellite.charAt(1);
            List<List<Double>> measurements = getMeasurements(firstDigit, secondDigit, 2);
            if (measurements.size() >= amountOfObservations) {
                String number = (String.valueOf(firstDigit) + String.valueOf(secondDigit));
                satellites.add(number);
            }
        }
        return satellites;
    }

    public String getElevationAngles(char requiredSatelliteNumber1, char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
        StringBuilder elevationAnglesArray = new StringBuilder();
        List<List<Double>> measurements = getMeasurements(requiredSatelliteNumber1, requiredSatelliteNumber2, requiredSatelliteNumberSize);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            if (observation % 10 == 0)
                elevationAnglesArray.append("\n");
            double elevationAngle = measurements.get(observation).get(14);
            elevationAnglesArray.append(elevationAngle).append(", ");
        }
        String elevationAngles = elevationAnglesArray.toString();
        return elevationAngles;
    }

    public String getElevationAngles(char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
        StringBuilder elevationAnglesArray = new StringBuilder();
        List<List<Double>> measurements = getMeasurements(requiredSatelliteNumber, requiredSatelliteNumberSize);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            if (observation % 10 == 0)
                elevationAnglesArray.append("\n");

            double elevationAngle = measurements.get(observation).get(14);
            elevationAnglesArray.append(elevationAngle).append(", ");
        }
        String elevationAngles = elevationAnglesArray.toString();
        return elevationAngles;
    }

    private List<List<Double>> getMeasurements(char requiredSatelliteNumber1, char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
        List<List<Double>> measurements = new ArrayList<>();
        int numbersInLine = 21;
        for (List<List<Byte>> line : allLines) {
            try {
                char satelliteNumber1 = (char)(byte)line.get(0).get(0);
                char satelliteNumber2 = (char)(byte)line.get(0).get(1);
                int satelliteNumberSize = line.get(0).size();

                if ((satelliteNumber1 == requiredSatelliteNumber1) &&
                    (satelliteNumber2 == requiredSatelliteNumber2) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    List<Double> lineOfNumbers = new ArrayList<>();
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.add(numeric);
                    }
                    measurements.add(lineOfNumbers);
                }
            } catch (Exception ignored) { }
        }
        return measurements;
    }

    private List<List<Double>> getMeasurements(char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
        List<List<Double>> measurements = new ArrayList<>();
        int numbersInLine = 21;

        for (List<List<Byte>> line : allLines) {
            try {
                char satelliteNumber = (char)(byte)line.get(0).get(0);
                int satelliteNumberSize = line.get(0).size();

                if ((satelliteNumber == requiredSatelliteNumber) && (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    List<Double> lineOfNumbers = new ArrayList<>();
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.add(numeric);
                    }
                    measurements.add(lineOfNumbers);
                }
            } catch (Exception ignored) { }
        }
        return measurements;
    }

    private double getNumeric(List<List<Byte>> line, int number) {
        StringBuilder numberBuilder = new StringBuilder();
        int numberLength = line.get(number).size();

        for (int digit = 0; digit < numberLength; digit++) {
            char symbol = (char)line.get(number).get(digit).byteValue();
            numberBuilder.append(symbol);
        }

        double numeric = Double.parseDouble(numberBuilder.toString());
        return numeric;
    }

    private List<List<List<Byte>>> analyzeSyntaxAndReturnLinesList() {
        List<List<List<Byte>>> allLines = new ArrayList<>();
        List<List<Byte>> words = new ArrayList<>();
        List<Byte> symbols = new ArrayList<>();
        boolean isWord = false;

        byte tab = 9;
        byte newLine = 10;
        byte carriageReturn = 13;
        byte space = 32;

        for (byte symbol : fileBytes) {
            if (symbol == newLine) {
                words.add(symbols);
                symbols = new ArrayList<>();
                allLines.add(words);
                words = new ArrayList<>();
                isWord = false;
            } else if ((isWord) && (symbol == tab)) {
                words.add(symbols);
                symbols = new ArrayList<>();
                isWord = false;
            } else if ((symbol != space) && (symbol != tab) && (symbol != carriageReturn)) {
                isWord = true;
                symbols.add(symbol);
            }
        }
        return allLines;
    }
}
