package com.gvozdev.codegen.exercise.filereaders;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Var3EphemerisFileReader {
    private final byte[] fileBytes;
    private final List<List<List<Byte>>> allLines;
    private final int amountOfObservations;

    public Var3EphemerisFileReader() throws IOException {
        String ephemerisFileName = "src/main/resources/files/input/brdc0010.18n";
        FileInputStream fin = new FileInputStream(ephemerisFileName);
        fileBytes = new byte[fin.available()];
        int offset = 0;
        fin.read(fileBytes, offset, fileBytes.length);
        fin.close();
        allLines = analyzeSyntaxAndReturnLinesList();
        amountOfObservations = 12;
    }

    public double[] getGpsTime(char requiredSatelliteNumber1, char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
        double[] gpsTime = new double[amountOfObservations];
        int startOfObservations = 8;
        int linesPerObservation = 8;

        for (int observation = startOfObservations; observation < allLines.size(); observation += linesPerObservation) {
            try {
                char satelliteNumber1 = (char)(byte)allLines.get(observation).get(0).get(0);
                char satelliteNumber2 = (char)(byte)allLines.get(observation).get(0).get(1);
                int satelliteNumberSize = allLines.get(observation).get(0).size();
                int hourFirstNumber = Character.getNumericValue(allLines.get(observation).get(4).get(0));

                if ((allLines.get(observation).get(4).size() == 1) &&
                    (hourFirstNumber % 2 == 0) &&
                    (satelliteNumber1 == requiredSatelliteNumber1) &&
                    (satelliteNumber2 == requiredSatelliteNumber2) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    double numeric = getNumericGpsTime(observation);
                    gpsTime[hourFirstNumber / 2] = numeric;
                } else {
                    int hourSecondNumber = Character.getNumericValue(allLines.get(observation).get(4).get(1));
                    String hourFull = hourFirstNumber + Integer.toString(hourSecondNumber);
                    int hourValue = Integer.parseInt(hourFull);

                    if ((hourValue % 2 == 0) &&
                        (satelliteNumber1 == requiredSatelliteNumber1) &&
                        (satelliteNumber2 == requiredSatelliteNumber2) &&
                        (satelliteNumberSize == requiredSatelliteNumberSize)) {
                        double numeric = getNumericGpsTime(observation);
                        gpsTime[hourValue / 2] = numeric;
                    }
                }
            } catch (Exception ignored) { }
        }
        return gpsTime;
    }

    public double[] getGpsTime(char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
        double[] gpsTime = new double[amountOfObservations];
        int startOfObservations = 8;
        int linesPerObservation = 8;

        for (int observation = startOfObservations; observation < allLines.size(); observation += linesPerObservation) {
            try {
                char satelliteNumber = (char)(byte)allLines.get(observation).get(0).get(0);
                int satelliteNumberSize = allLines.get(observation).get(0).size();
                int hourFirstNumber = Character.getNumericValue(allLines.get(observation).get(4).get(0));

                if ((allLines.get(observation).get(4).size() == 1) &&
                    (hourFirstNumber % 2 == 0) &&
                    (satelliteNumber == requiredSatelliteNumber) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    double numeric = getNumericGpsTime(observation);
                    gpsTime[hourFirstNumber / 2] = numeric;
                } else {
                    int hourSecondNumber = Character.getNumericValue(allLines.get(observation).get(4).get(1));
                    String hourFull = hourFirstNumber + Integer.toString(hourSecondNumber);
                    int hourValue = Integer.parseInt(hourFull);

                    if ((hourValue % 2 == 0) &&
                        (satelliteNumber == requiredSatelliteNumber) &&
                        (satelliteNumberSize == requiredSatelliteNumberSize)) {
                        double numeric = getNumericGpsTime(observation);
                        gpsTime[hourValue / 2] = numeric;
                    }
                }
            } catch (Exception ignored) { }
        }
        return gpsTime;
    }

    private double getNumericGpsTime(int observation) {
        StringBuilder numberBuilder = new StringBuilder();
        int observationOffset = 7;
        int digits = allLines.get(observation + observationOffset).get(0).size();

        for (int digit = 0; digit < digits; digit++) {
            char symbol = (char)allLines.get(observation + observationOffset).get(0).get(digit).byteValue();

            if (symbol == 'D')
                numberBuilder.append('E');
            else
                numberBuilder.append(symbol);
        }
        double numeric = Double.parseDouble(numberBuilder.toString());
        return numeric;
    }

    public String getAlpha() {
        String alpha = extractIonCoefficients(3);
        return alpha;
    }

    public String getBeta() {
        String beta = extractIonCoefficients(4);
        return beta;
    }

    private String extractIonCoefficients(int lineNumber) {
        List<List<Byte>> line = allLines.get(lineNumber);
        StringBuilder builder = new StringBuilder();
        int amountOfCoefficients = 4;
        for (int coefficient = 0; coefficient < amountOfCoefficients; coefficient++) {
            double numeric = getNumericCoefficient(line, coefficient);
            builder.append(numeric).append(", ");
        }
        String coefficients = builder.toString();
        return coefficients;
    }

    private double getNumericCoefficient(List<List<Byte>> line, int number) {
        StringBuilder numberBuilder = new StringBuilder();
        int digits = line.get(number).size();
        for (int digit = 0; digit < digits; digit++) {
            char symbol = (char)line.get(number).get(digit).byteValue();
            if (symbol == 'D')
                numberBuilder.append('E');
            else
                numberBuilder.append(symbol);
        }
        double numeric = Double.parseDouble(numberBuilder.toString());
        return numeric;
    }

    private List<List<List<Byte>>> analyzeSyntaxAndReturnLinesList() {
        List<List<List<Byte>>> allLines = new ArrayList<>();
        List<List<Byte>> words = new ArrayList<>();
        List<Byte> symbols = new ArrayList<>();
        boolean isWord = false;

        byte newLine = 10;
        byte space = 32;

        for (byte symbol : fileBytes) {
            if (symbol == newLine) {
                words.add(symbols);
                symbols = new ArrayList<>();
                allLines.add(words);
                words = new ArrayList<>();
                isWord = false;
            } else if ((isWord) && (symbol == space)) {
                words.add(symbols);
                symbols = new ArrayList<>();
                isWord = false;
            } else if (symbol != space) {
                isWord = true;
                symbols.add(symbol);
            }
        }
        return allLines;
    }
}
