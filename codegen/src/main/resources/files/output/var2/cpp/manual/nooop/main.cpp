﻿#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#include <iomanip>

std::vector<double> __getAngularVelocities(std::vector<double> elevationAngles, int amountOfObservations) {
	std::vector<double> angularVelocities;
	const double oneHourInSeconds = 3600;
	double previousElevationAngle = elevationAngles.at(0);
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double currentElevationAngle = elevationAngles.at(observation);
		double angularVelocity = (currentElevationAngle - previousElevationAngle) / oneHourInSeconds;
		angularVelocities.push_back(angularVelocity);
		previousElevationAngle = currentElevationAngle;
	}
	return angularVelocities;
}

double __getVelocitySumPerHour(std::vector<double> velocities, int start, int end) {
	double velocitySum = 0;
	for (int observation = start; observation < end; observation++) {
        velocitySum += velocities.at(observation);
	}
	return velocitySum;
}

std::vector<double> __getAverageAngularVelocities(std::vector<double> elevationAngles, int amountOfObservations) {
	std::vector<double> averageVelocities;
	std::vector<double> angularVelocities = __getAngularVelocities(elevationAngles, amountOfObservations);
	int observationsPerHour = 120;

	double firstHourSum = __getVelocitySumPerHour(angularVelocities, 0, 120);
	double secondHourSum = __getVelocitySumPerHour(angularVelocities, 120, 240);
	double thirdHourSum = __getVelocitySumPerHour(angularVelocities, 240, 360);

	double firstHourAverage = firstHourSum / observationsPerHour;
	double secondHourAverage = secondHourSum / observationsPerHour;
	double thirdHourAverage = thirdHourSum / observationsPerHour;

	averageVelocities.push_back(firstHourAverage);
	averageVelocities.push_back(secondHourAverage);
	averageVelocities.push_back(thirdHourAverage);
	return averageVelocities;
}

std::vector<double> __getLinearVelocities(int amountOfObservations) {
	const double gravitational = 6.67 * pow(10, -11);
	const double earthMass = 5.972E24;
	const double earthRadius = 6371000;
	const double height = 20000;
	std::vector<double> linearVelocities;

	for (int interval = 0; interval < amountOfObservations; interval++) {
		double linearVelocity = sqrt(gravitational * earthMass / (earthRadius + height));
		linearVelocities.push_back(linearVelocity);
	}
	return linearVelocities;
}

std::vector<double> __getAverageLinearVelocities(int amountOfObservations) {
	std::vector<double> averageVelocities;
	std::vector<double> linearVelocities = __getLinearVelocities(amountOfObservations);
	int observationsPerHour = 120;

	double firstHourSum = __getVelocitySumPerHour(linearVelocities, 0, 120);
	double secondHourSum = __getVelocitySumPerHour(linearVelocities, 120, 240);
	double thirdHourSum = __getVelocitySumPerHour(linearVelocities, 240, 360);

	double firstHourAverage = firstHourSum / observationsPerHour;
	double secondHourAverage = secondHourSum / observationsPerHour;
	double thirdHourAverage = thirdHourSum / observationsPerHour;

	averageVelocities.push_back(firstHourAverage);
	averageVelocities.push_back(secondHourAverage);
	averageVelocities.push_back(thirdHourAverage);
	return averageVelocities;
}

void __printAngularVelocities(int satellite1Number, int satellite2Number, int satellite3Number,
                              std::vector<double> satellite1ElevationAngles, std::vector<double> satellite2ElevationAngles,
                              std::vector<double> satellite3ElevationAngles, int amountOfObservations) {
	std::cout << "Угловая скорость\nСпутник #" << satellite1Number << "\tСпутник #" << satellite2Number <<
                 "\tСпутник #" << satellite3Number << std::endl;
	std::cout << std::fixed << std::setprecision(10);
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double satellite1Velocity = __getAngularVelocities(satellite1ElevationAngles, amountOfObservations).at(observation);
		double satellite2Velocity = __getAngularVelocities(satellite2ElevationAngles, amountOfObservations).at(observation);
		double satellite3Velocity = __getAngularVelocities(satellite3ElevationAngles, amountOfObservations).at(observation);
		std::cout << satellite1Velocity << "\t" << satellite2Velocity << "\t" << satellite3Velocity << std::endl;
	}
	std::cout << "***********************************************" << std::endl;
}

void __printAverageAngularVelocities(int satellite1Number, int satellite2Number, int satellite3Number,
                                     std::vector<double> satellite1ElevationAngles, std::vector<double> satellite2ElevationAngles,
                                     std::vector<double> satellite3ElevationAngles, int amountOfObservations) {
	std::cout << "Средняя угловая скорость\nСпутник #" << satellite1Number << "\t\tСпутник #" << satellite2Number <<
                 "\t\tСпутник #" << satellite3Number << std::endl;
	for (int hour = 0; hour < 3; hour++) {
		double satellite1Velocity = __getAverageAngularVelocities(satellite1ElevationAngles, amountOfObservations).at(hour);
		double satellite2Velocity = __getAverageAngularVelocities(satellite2ElevationAngles, amountOfObservations).at(hour);
		double satellite3Velocity = __getAverageAngularVelocities(satellite3ElevationAngles, amountOfObservations).at(hour);
		std::cout << satellite1Velocity << "\t" << satellite2Velocity << "\t" << satellite3Velocity << std::endl;
	}
	std::cout << "***********************************************" << std::endl;
}

void __printLinearVelocities(int satellite1Number, int satellite2Number, int satellite3Number, int amountOfObservations) {
	std::cout << "Линейная скорость\nСпутник #" << satellite1Number << "\tСпутник #" << satellite2Number <<
                 "\tСпутник #" << satellite3Number << std::endl;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double satellite1Velocity = __getLinearVelocities(amountOfObservations).at(observation);
		double satellite2Velocity = __getLinearVelocities(amountOfObservations).at(observation);
		double satellite3Velocity = __getLinearVelocities(amountOfObservations).at(observation);
		std::cout << satellite1Velocity << "\t" << satellite2Velocity << "\t" << satellite3Velocity << std::endl;
	}
	std::cout << "***********************************************" << std::endl;
}

void __printAverageLinearVelocities(int satellite1Number, int satellite2Number, int satellite3Number, int amountOfObservations) {
	std::cout << "Средняя линейная скорость\nСпутник #" << satellite1Number << "\t\tСпутник #" << satellite2Number <<
                 "\t\tСпутник #" << satellite3Number << std::endl;
	for (int hour = 0; hour < 3; hour++) {
		double satellite1Velocity = __getAverageLinearVelocities(amountOfObservations).at(hour);
		double satellite2Velocity = __getAverageLinearVelocities(amountOfObservations).at(hour);
		double satellite3Velocity = __getAverageLinearVelocities(amountOfObservations).at(hour);
		std::cout << satellite1Velocity << "\t" << satellite2Velocity << "\t" << satellite3Velocity << std::endl;
	}
	std::cout << "***********************************************" << std::endl;
}

void getConsoleOutput(int satellite1Number, int satellite2Number, int satellite3Number,
                      std::vector<double> satellite1ElevationAngles, std::vector<double> satellite2ElevationAngles,
                      std::vector<double> satellite3ElevationAngles, int amountOfObservations) {
	__printAngularVelocities(satellite1Number, satellite2Number, satellite3Number, satellite1ElevationAngles, satellite2ElevationAngles,
	                         satellite3ElevationAngles, amountOfObservations);
	__printAverageAngularVelocities(satellite1Number, satellite2Number, satellite3Number, satellite1ElevationAngles, satellite2ElevationAngles,
	                                satellite3ElevationAngles, amountOfObservations);
	__printLinearVelocities(satellite1Number, satellite2Number, satellite3Number, amountOfObservations);
	__printAverageLinearVelocities(satellite1Number, satellite2Number, satellite3Number, amountOfObservations);
}

int main() {
    setlocale(LC_ALL, "rus");
	const int amountOfObservations = 360;

    std::vector<double> satellite1ElevationAngles { ? };

    std::vector<double> satellite2ElevationAngles { ? };

    std::vector<double> satellite3ElevationAngles { ? };

	getConsoleOutput(?, ?, ?, satellite1ElevationAngles, satellite2ElevationAngles, satellite3ElevationAngles, amountOfObservations);
}