package Default;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        int amountOfObservations = 12;

        AxisGeo axisGeo = new AxisGeo(UserGeo.lonpp, IGPGeo.lon1, IGPGeo.lon2, UserGeo.latpp, IGPGeo.lat1, IGPGeo.lat2);
        WeightMatrix weightMatrix = new WeightMatrix(axisGeo);

        String fileNameEphemeris = "./src/resources/brdc0010.18n";
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(amountOfObservations, fileNameEphemeris);
        IonCoefficients alpha = new IonCoefficients(ephemerisFileReader.getAlpha());
        IonCoefficients beta = new IonCoefficients(ephemerisFileReader.getBeta());
        GpsTime gpsTime = new GpsTime(ephemerisFileReader.getGpsTime('7', 1));

        String fileNameForecast = "./src/resources/igrg0010.18i";
        IonoFileReader ionoFileReaderForecast = new IonoFileReader(fileNameForecast);

        List<List<List<Integer>>> forecastA1 = ionoFileReaderForecast.getTecArray('5', '7', '.', 304);
        List<List<List<Integer>>> forecastA2 = ionoFileReaderForecast.getTecArray('5', '7', '.', 304);
        List<List<List<Integer>>> forecastA3 = ionoFileReaderForecast.getTecArray('5', '5', '.', 304);
        List<List<List<Integer>>> forecastA4 = ionoFileReaderForecast.getTecArray('5', '5', '.', 304);

        String fileNameReal = "./src/resources/igsg0010.18i";
        IonoFileReader ionoFileReaderReal = new IonoFileReader(fileNameReal);
        List<List<List<Integer>>> preciseA1 = ionoFileReaderReal.getTecArray('5', '7', '.', 394);
        List<List<List<Integer>>> preciseA2 = ionoFileReaderReal.getTecArray('5', '7', '.', 394);
        List<List<List<Integer>>> preciseA3 = ionoFileReaderReal.getTecArray('5', '5', '.', 394);
        List<List<List<Integer>>> preciseA4 = ionoFileReaderReal.getTecArray('5', '5', '.', 394);

        IonosphericDelaysFactory ionosphericDelaysFactory = new IonosphericDelaysFactory(weightMatrix, 95, 90, 90, 95, amountOfObservations);
        KlobucharDelaysFactory klobucharDelaysFactory = new KlobucharDelaysFactory(gpsTime, alpha, beta, amountOfObservations);

        List<IonosphericDelay> forecastDelays = ionosphericDelaysFactory.createDelays(forecastA1, forecastA2, forecastA3, forecastA4);
        List<IonosphericDelay> preciseDelays = ionosphericDelaysFactory.createDelays(preciseA1, preciseA2, preciseA3, preciseA4);
        List<KlobucharModel> klobucharDelays = klobucharDelaysFactory.createKlobuchar();

        ConsoleOutput consoleOutput = new ConsoleOutput(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations);
        consoleOutput.printDelays();

        GraphDrawer.draw();
    }
}


class UserGeo {
    private static final double halfCircle = 180;
    public static double latpp = 56.010569 / halfCircle;
    public static double lonpp = 92.852545 / halfCircle;
}


class IGPGeo {
    private static final double halfCircle = 180;
    public static double lon1 = 90 / halfCircle;
    public static double lon2 = 95 / halfCircle;
    public static double lat1 = 55 / halfCircle;
    public static double lat2 = 57.5 / halfCircle;
}


class AxisGeo {
    private final double xpp;
    private final double ypp;

    public AxisGeo(double lonpp, double lon1, double lon2, double latpp, double lat1, double lat2) {
        this.xpp = (lonpp - lon1) / (lon2 - lon1);
        this.ypp = (latpp - lat1) / (lat2 - lat1);
    }

    public double getXpp() {
        return xpp;
    }

    public double getYpp() {
        return ypp;
    }
}


class WeightMatrix {
    private final List<Double> weights;

    public WeightMatrix(AxisGeo axisGeo) {
        this.weights = new ArrayList<>(4);
        double xpp = axisGeo.getXpp();
        double ypp = axisGeo.getYpp();
        weights.add(xpp * ypp);
        weights.add((1 - xpp) * ypp);
        weights.add((1 - xpp) * (1 - ypp));
        weights.add(xpp * (1 - ypp));
    }

    public double getWeightAt(int pos) {
        return weights.get(pos);
    }
}


class ConsoleOutput {
    private final List<IonosphericDelay> forecastDelays;
    private final List<IonosphericDelay> preciseDelays;
    private final List<KlobucharModel> klobucharDelays;
    private final int amountOfObservations;

    public ConsoleOutput(List<IonosphericDelay> forecastDelays, List<IonosphericDelay> preciseDelays, List<KlobucharModel> klobucharDelays,
                         int amountOfObservations) {
        this.forecastDelays = forecastDelays;
        this.preciseDelays = preciseDelays;
        this.klobucharDelays = klobucharDelays;
        this.amountOfObservations = amountOfObservations;
    }

    public void printDelays() {
        System.out.println("igrg\tigsg\tklobuchar");
        DecimalFormat df = new DecimalFormat("#.###");
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double forecastValue = forecastDelays.get(observation).getDelayInMeters();
            double preciseValue = preciseDelays.get(observation).getDelayInMeters();
            double klobucharValue = klobucharDelays.get(observation).getKlobucharDelayInMeters();
            System.out.println(df.format(forecastValue) + "\t" + df.format(preciseValue) + "\t" + klobucharValue);
        }
    }
}


class IonosphericDelaysFactory {
    private final WeightMatrix weightMatrix;
    private final int lonFirst = -180;
    private final int dlon = 5;
    private final int tecValuesPerLine = 16;
    private final int lon1;
    private final int lon2;
    private final int lon3;
    private final int lon4;
    private final int amountOfObservations;
    private int rowA1, rowA2, rowA3, rowA4, posA1, posA2, posA3, posA4;

    public IonosphericDelaysFactory(WeightMatrix weightMatrix, int lon1, int lon2, int lon3, int lon4, int amountOfObservations) {
        this.amountOfObservations = amountOfObservations;
        this.weightMatrix = weightMatrix;
        this.lon1 = lon1;
        this.lon2 = lon2;
        this.lon3 = lon3;
        this.lon4 = lon4;
    }

    public List<IonosphericDelay> createDelays(List<List<List<Integer>>> tecA1,
        List<List<List<Integer>>> tecA2,
        List<List<List<Integer>>> tecA3,
        List<List<List<Integer>>> tecA4) {
        List<IonosphericDelay> delays = new ArrayList<>(amountOfObservations);
        setRows();
        setPos();
        for (int observation = 0; observation < amountOfObservations; observation++) {
            int a1 = tecA1.get(observation).get(rowA1).get(posA1);
            int a2 = tecA2.get(observation).get(rowA2).get(posA2);
            int a3 = tecA3.get(observation).get(rowA3).get(posA3);
            int a4 = tecA4.get(observation).get(rowA4).get(posA4);
            List<Integer> tecArray = new ArrayList<>();

            tecArray.add(a1);
            tecArray.add(a2);
            tecArray.add(a3);
            tecArray.add(a4);

            Tec tempTec = new Tec(tecArray);
            IonosphericDelay tempDelay = new IonosphericDelay(weightMatrix, tempTec);
            delays.add(tempDelay);
        }
        return delays;
    }

    private void setPos() {
        posA1 = getPos(rowA1, lon1);
        posA2 = getPos(rowA2, lon2);
        posA3 = getPos(rowA3, lon3);
        posA4 = getPos(rowA4, lon4);
    }

    private int getPos(int row, int lon) {
        int number = (Math.abs((lonFirst - lon) / dlon)) - (row * tecValuesPerLine);
        return number;
    }

    private void setRows() {
        rowA1 = getRow(lon1);
        rowA2 = getRow(lon2);
        rowA3 = getRow(lon3);
        rowA4 = getRow(lon4);
    }

    private int getRow(int lon) {
        int row = Math.abs((lonFirst - lon) / (tecValuesPerLine * dlon));
        return row;
    }
}


class KlobucharDelaysFactory {
    private final GpsTime gpsTime;
    private final IonCoefficients alpha;
    private final IonCoefficients beta;
    private final int amountOfObservations;

    public KlobucharDelaysFactory(GpsTime gpsTime, IonCoefficients alpha, IonCoefficients beta, int amountOfObservations) {
        this.gpsTime = gpsTime;
        this.alpha = alpha;
        this.beta = beta;
        this.amountOfObservations = amountOfObservations;
    }

    public List<KlobucharModel> createKlobuchar() {
        List<KlobucharModel> models = new ArrayList<>(amountOfObservations);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double time = gpsTime.getGpsTimeAt(observation);
            KlobucharModel klobucharTemp = new KlobucharModel(time, alpha, beta);
            models.add(klobucharTemp);
        }
        return models;
    }
}


class IonCoefficients {
    private final List<Double> coefficients;

    public IonCoefficients(List<Double> coefficients) {
        this.coefficients = coefficients;
    }

    public double getCoefficientAt(int pos) {
        return coefficients.get(pos);
    }
}


class Tec {
    private final List<Integer> tec;

    public Tec(List<Integer> tec) {
        this.tec = tec;
    }

    public double getTecAt(int pos) {
        return tec.get(pos);
    }
}


class GpsTime {
    private final double[] gpsTime;

    public GpsTime(double[] gpsTime) {
        this.gpsTime = gpsTime;
    }

    public double getGpsTimeAt(int pos) {
        return gpsTime[pos];
    }
}


class IonosphericDelay {
    private final WeightMatrix weightMatrix;
    private final Tec tec;

    public IonosphericDelay(WeightMatrix weightMatrix, Tec tec) {
        this.weightMatrix = weightMatrix;
        this.tec = tec;
    }

    public double getDelayInMeters() {
        double tecuToMetersCoefficient = getTecuToMetersCoefficient();
        double delayInTecu = getDelayInTecu();
        double delayInMeters = delayInTecu * tecuToMetersCoefficient;
        return delayInMeters;
    }

    private double getTecuToMetersCoefficient() {
        double l1 = 1_575_420_000;
        double oneTecUnit = 1E16;
        double coefficient = 40.3 / Math.pow(l1, 2) * oneTecUnit;
        return coefficient;
    }

    private double getDelayInTecu() {
        double delay = 0;
        for (int observation = 0; observation < 4; observation++) {
            double weight = weightMatrix.getWeightAt(observation);
            double rawTec = tec.getTecAt(observation);
            double tecInOneTecUnit = rawTec * 0.1;
            delay += (weight * tecInOneTecUnit);
        }
        return delay;
    }
}


class KlobucharModel {
    private final double elevationAngle;
    private final double azimuth;
    private final double gpsTime;
    private final IonCoefficients alpha;
    private final IonCoefficients beta;

    public KlobucharModel(double gpsTime, IonCoefficients alpha, IonCoefficients beta) {
        double halfCircle = 180;
        this.gpsTime = gpsTime;
        this.elevationAngle = 90 / halfCircle;
        this.azimuth = 0;
        this.alpha = alpha;
        this.beta = beta;
    }

    public double getKlobucharDelayInMeters() {
        double delayInSeconds = getKlobucharDelayInSeconds();
        double speedOfLight = 2.99792458 * 1E8;
        double delayInMeters = delayInSeconds * speedOfLight;
        return delayInMeters;
    }

    private double getEarthCenteredAngle() {
        double earthCenteredAngle = 0.0137 / (elevationAngle + 0.11) - 0.022;
        return earthCenteredAngle;
    }

    private double getIppLatitude() {
        double latpp = UserGeo.latpp;
        double earthCenteredAngle = getEarthCenteredAngle();
        double ippLatitude = latpp + earthCenteredAngle * Math.cos(azimuth);
        if (ippLatitude > 0.416)
            ippLatitude = 0.416;
        else if (ippLatitude < -0.416)
            ippLatitude = -0.416;
        return ippLatitude;
    }

    private double getIppLongtitude() {
        double lonpp = UserGeo.lonpp;
        double earthCenteredAngle = getEarthCenteredAngle();
        double ippLatitude = getIppLatitude();
        double ippLongtitude = lonpp + (earthCenteredAngle * Math.sin(azimuth) / (Math.cos(ippLatitude)));
        return ippLongtitude;
    }

    private double getIppGeomagneticLatitude() {
        double ippLatitude = getIppLatitude();
        double ippLongtitude = getIppLongtitude();
        double ippGeomagneticLatitude = ippLatitude + 0.064 * Math.cos(ippLongtitude - 1.617);
        return ippGeomagneticLatitude;
    }

    private double getIppLocalTime() {
        double secondsInOneDay = 86_400;
        double secondsInTwelveHours = 43_200;
        double ippLongtitude = getIppLongtitude();
        double ippLocalTime = secondsInTwelveHours * ippLongtitude + gpsTime;
        while (ippLocalTime > secondsInOneDay)
            ippLocalTime -= secondsInOneDay;
        while (ippLocalTime < 0)
            ippLocalTime += secondsInOneDay;
        return ippLocalTime;
    }

    private double getIonosphericDelayAmplitude() {
        double ippGeomagneticLatitude = getIppGeomagneticLatitude();
        double amplitude = 0;
        for (int i = 0; i < 4; i++) {
            amplitude += (alpha.getCoefficientAt(i) * Math.pow(ippGeomagneticLatitude, i));
        }
        if (amplitude < 0)
            amplitude = 0;
        return amplitude;
    }

    private double getIonosphericDelayPeriod() {
        double ippGeomagneticLatitude = getIppGeomagneticLatitude();
        double period = 0;
        for (int i = 0; i < 4; i++) {
            period += (beta.getCoefficientAt(i) * Math.pow(ippGeomagneticLatitude, i));
        }
        if (period < 72_000)
            period = 72_000;
        return period;
    }

    private double getIonosphericDelayPhase() {
        double ippLocalTime = getIppLocalTime();
        double ionosphericDelayPeriod = getIonosphericDelayPeriod();
        double ionosphericDelayPhase = 2 * Math.PI * (ippLocalTime - 50_400) / ionosphericDelayPeriod;
        return ionosphericDelayPhase;
    }

    private double getSlantFactor() {
        double slantFactor = 1 + 16 * Math.pow((0.53 - elevationAngle), 3);
        return slantFactor;
    }

    private double getKlobucharDelayInSeconds() {
        double ionosphericDelayPhase = getIonosphericDelayPhase();
        double ionosphericDelayAmplitude = getIonosphericDelayAmplitude();
        double slantFactor = getSlantFactor();
        double ionosphericTimeDelay;
        if (Math.abs(ionosphericDelayPhase) > 1.57)
            ionosphericTimeDelay = 5E-9 * slantFactor;
        else
            ionosphericTimeDelay = (5E-9 + ionosphericDelayAmplitude * (1 - Math.pow(ionosphericDelayPhase, 2) / 2 + Math.pow(ionosphericDelayPhase, 4) / 24)) * slantFactor;
        return ionosphericTimeDelay;
    }
}


class EphemerisFileReader {
    private final int amountOfObservations;
    private final byte[] allBytes;
    private final List<List<List<Byte>>> allLines;

    public EphemerisFileReader(int amountOfObservations, String fileName) throws IOException {
        this.amountOfObservations = amountOfObservations;
        FileInputStream fin = new FileInputStream(fileName);
        allBytes = new byte[fin.available()];
        int offset = 0;
        fin.read(allBytes, offset, allBytes.length);
        allLines = analyzeSyntaxAndReturnLinesList();
    }

    public List<Double> getAlpha() {
        List<Double> alpha = extractIonCoeffs(3);
        return alpha;
    }

    public List<Double> getBeta() {
        List<Double> beta = extractIonCoeffs(4);
        return beta;
    }

    private List<Double> extractIonCoefficients(int lineNumber) {
        List<List<Byte>> line = allLines.get(lineNumber);
        List<Double> coefficients = new ArrayList<>();
        int amountOfCoefficients = 4;

        for (int coefficient = 0; coefficient < amountOfCoefficients; coefficient++) {
            double numeric = getNumericCoefficient(line, coefficient);
            coefficients.add(numeric);
        }
        return coefficients;
    }

    private double getNumericCoefficient(List<List<Byte>> line, int number) {
        StringBuilder numberBuilder = new StringBuilder();
        int digits = line.get(number).size();

        for (int digit = 0; digit < digits; digit++) {
            char symbol = (char)line.get(number).get(digit).byteValue();
            if (symbol == 'D')
                numberBuilder.append('E');
            else
                numberBuilder.append(symbol);
        }
        double numeric = Double.parseDouble(numberBuilder.toString());
        return numeric;
    }

    public double[] getGpsTime(char requiredSatelliteNumber1, char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
        double[] gpsTime = new double[amountOfObservations];
        int startOfObservations = 8;
        int linesPerObservation = 8;

        for (int observation = startOfObservations; observation < allLines.size(); observation += linesPerObservation) {
            try {
                char satelliteNumber1 = (char)(byte)allLines.get(observation).get(0).get(0);
                char satelliteNumber2 = (char)(byte)allLines.get(observation).get(0).get(1);
                int satelliteNumberSize = allLines.get(observation).get(0).size();
                int hourFirstNumber = Character.getNumericValue(allLines.get(observation).get(4).get(0));

                if ((allLines.get(observation).get(4).size() == 1) &&
                    (hourFirstNumber % 2 == 0) &&
                    (satelliteNumber1 == requiredSatelliteNumber1) &&
                    (satelliteNumber2 == requiredSatelliteNumber2) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    double numeric = getGpsTimeNumeric(observation);
                    gpsTime[hourFirstNumber / 2] = numeric;
                } else {
                    int hourSecondNumber = Character.getNumericValue(allLines.get(observation).get(4).get(1));
                    String hourFull = hourFirstNumber + Integer.toString(hourSecondNumber);
                    int hourValue = Integer.parseInt(hourFull);

                    if ((hourValue % 2 == 0) &&
                        (satelliteNumber1 == requiredSatelliteNumber1) &&
                        (satelliteNumber2 == requiredSatelliteNumber2) &&
                        (satelliteNumberSize == requiredSatelliteNumberSize)) {
                        double numeric = getGpsTimeNumeric(observation);
                        gpsTime[hourValue / 2] = numeric;
                    }
                }
            } catch (Exception ignored) { }
        }
        return gpsTime;
    }

    public double[] getGpsTime(char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
        double[] gpsTime = new double[amountOfObservations];
        int startOfObservations = 8;
        int linesPerObservation = 8;

        for (int observation = startOfObservations; observation < allLines.size(); observation += linesPerObservation) {
            try {
                char satelliteNumber = (char)(byte)allLines.get(observation).get(0).get(0);
                int satelliteNumberSize = allLines.get(observation).get(0).size();
                int hourFirstNumber = Character.getNumericValue(allLines.get(observation).get(4).get(0));

                if ((allLines.get(observation).get(4).size() == 1) &&
                    (hourFirstNumber % 2 == 0) &&
                    (satelliteNumber == requiredSatelliteNumber) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    double numeric = getGpsTimeNumeric(observation);
                    gpsTime[hourFirstNumber / 2] = numeric;
                } else {
                    int hourSecondNumber = Character.getNumericValue(allLines.get(observation).get(4).get(1));
                    String hourFull = hourFirstNumber + Integer.toString(hourSecondNumber);
                    int hourValue = Integer.parseInt(hourFull);

                    if ((hourValue % 2 == 0) &&
                        (satelliteNumber == requiredSatelliteNumber) &&
                        (satelliteNumberSize == requiredSatelliteNumberSize)) {
                        double numeric = getGpsTimeNumeric(observation);
                        gpsTime[hourValue / 2] = numeric;
                    }
                }
            } catch (Exception ignored) { }
        }
        return gpsTime;
    }

    private double getGpsTimeNumeric(int observation) {
        StringBuilder numberBuilder = new StringBuilder();
        int observationOffset = 7;
        int digits = allLines.get(observation + observationOffset).get(0).size();

        for (int digit = 0; digit < digits; digit++) {
            char symbol = (char)allLines.get(observation + observationOffset).get(0).get(digit).byteValue();

            if (symbol == 'D')
                numberBuilder.append('E');
            else
                numberBuilder.append(symbol);
        }
        double numeric = Double.parseDouble(numberBuilder.toString());
        return numeric;
    }

    private List<List<List<Byte>>> analyzeSyntaxAndReturnLinesList() {
        List<List<List<Byte>>> allLines = new ArrayList<>();
        List<List<Byte>> words = new ArrayList<>();
        List<Byte> symbols = new ArrayList<>();
        boolean isWord = false;

        byte newLine = 10;
        byte space = 32;

        for (byte symbol : allBytes) {
            if (symbol == newLine) {
                words.add(symbols);
                symbols = new ArrayList<>();
                allLines.add(words);
                words = new ArrayList<>();
                isWord = false;
            } else if ((isWord) && (symbol == space)) {
                words.add(symbols);
                symbols = new ArrayList<>();
                isWord = false;
            } else if (symbol != space) {
                isWord = true;
                symbols.add(symbol);
            }
        }
        return allLines;
    }
}


class IonoFileReader {
    private final byte[] allBytes;
    private final List<List<List<Byte>>> allLines;

    public IonoFileReader(String fileName) throws IOException {
        FileInputStream fin = new FileInputStream(fileName);
        allBytes = new byte[fin.available()];
        int offset = 0;
        fin.read(allBytes, offset, allBytes.length);
        allLines = analyzeSyntaxAndReturnLinesList();
    }

    public List<List<List<Integer>>> getTecArray(char requiredFirstLatDigit, char requiredSecondLatDigit, char requiredThirdLatDigit, int firstLine) {
        List<List<List<Integer>>> tecArray = new ArrayList<>();

        for (int line = firstLine; line < allLines.size(); line++) {
            try {
                char firstDigitOfLat = (char)(byte)allLines.get(line).get(0).get(0);
                char secondDigitOfLat = (char)(byte)allLines.get(line).get(0).get(1);
                char thirdDigitOfLat = (char)(byte)allLines.get(line).get(0).get(2);
                int linesWithTecPerLat = 5;

                if ((firstDigitOfLat == requiredFirstLatDigit) &&
                    (secondDigitOfLat == requiredSecondLatDigit) &&
                    (thirdDigitOfLat == requiredThirdLatDigit)) {
                    List<List<Integer>> tecPerLat = new ArrayList<>();
                    for (int lineWithTec = 1; lineWithTec <= linesWithTecPerLat; lineWithTec++) {
                        List<Integer> numbersLine = getNumberLine(line, lineWithTec);
                        tecPerLat.add(numbersLine);
                    }
                    tecArray.add(tecPerLat);
                }
            } catch (Exception ignored) { }
        }
        return tecArray;
    }

    private List<Integer> getNumberLine(int line, int lineWithTec) {
        List<Integer> numbersLine = new ArrayList<>();
        int numbersInRow = allLines.get(line + lineWithTec).size();
        for (int number = 0; number < numbersInRow; number++) {
            int numeric = getNumeric(line, lineWithTec, number);
            numbersLine.add(numeric);
        }
        return numbersLine;
    }

    private int getNumeric(int line, int lineWithTec, int number) {
        StringBuilder numberBuilder = new StringBuilder();
        int numberLength = allLines.get(line + lineWithTec).get(number).size();

        for (int digit = 0; digit < numberLength; digit++) {
            char symbol = (char)allLines.get(line + lineWithTec).get(number).get(digit).byteValue();
            numberBuilder.append(symbol);
        }
        int numeric = Integer.parseInt(numberBuilder.toString());
        return numeric;
    }

    private List<List<List<Byte>>> analyzeSyntaxAndReturnLinesList() {
        List<List<List<Byte>>> allLines = new ArrayList<>();
        List<List<Byte>> words = new ArrayList<>();
        List<Byte> symbols = new ArrayList<>();
        boolean isWord = false;

        int newLine = 10;
        int space = 32;

        for (byte symbol : allBytes) {
            if (symbol == newLine) {
                words.add(symbols);
                symbols = new ArrayList<>();
                allLines.add(words);
                words = new ArrayList<>();
                isWord = false;
            } else if ((isWord) && (symbol == space)) {
                words.add(symbols);
                symbols = new ArrayList<>();
                isWord = false;
            } else if (symbol != space) {
                isWord = true;
                symbols.add(symbol);
            }
        }
        return allLines;
    }
}