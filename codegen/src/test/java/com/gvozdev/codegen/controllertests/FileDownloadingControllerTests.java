package com.gvozdev.codegen.controllertests;

import com.gvozdev.codegen.filedownloading.FileDownloadingController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class FileDownloadingControllerTests {

    @Autowired
    private FileDownloadingController controller;

    @Test
    void fileDownloadingControllerLoads() throws Exception {
        assertThat(controller).isNotNull();
    }
}
